package greeting;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.ArrayList;

public class GreetingTest {
    @Test public void requirement1() {
        Greeting greeting = new Greeting();
        assertEquals("Hello, Bob.", greeting.greet("Bob"));
    }

    @Test public void requirement2() {
        Greeting greeting = new Greeting();
        assertEquals("Hello, my friend.", greeting.greet());
    }
    
    @Test public void requirement3() {
    	Greeting greeting = new Greeting();
    	assertEquals("HELLO JERRY!", greeting.greet("JERRY"));
    }

    @Test public void requirement4() {
    	Greeting greeting = new Greeting();
    	
    	ArrayList<String> people = new ArrayList<String>();
    	people.add("Jill");
    	people.add("Jane");
    	
    	assertEquals("Hello, Jill and Jane.", greeting.greet(people));
    }

    @Test public void requirement5() {
    	Greeting greeting = new Greeting();
    	
    	ArrayList<String> people = new ArrayList<String>();
    	people.add("Amy");
        people.add("Brian");
        people.add("Charlotte");
    	
    	assertEquals("Hello, Amy, Brian, and Charlotte.", greeting.greet(people));
    }

    @Test public void oneElementArrayTest() {
        Greeting greeting = new Greeting();
    	
        ArrayList<String> people = new ArrayList<String>();
        people.add("Bob");
        assertEquals("Hello, Bob.", greeting.greet(people));
        people.remove(0);

        people.add("JERRY");
        assertEquals("HELLO JERRY!", greeting.greet(people));
    }
}
