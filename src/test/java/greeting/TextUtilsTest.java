package greeting;

import org.junit.Test;
import static org.junit.Assert.*;

public class TextUtilsTest {
    @Test public void upperCaseTest() {
        assertEquals(false, TextUtils.isTextUpperCase("bruce"));
        assertEquals(false, TextUtils.isTextUpperCase("Bruce"));
        assertEquals(false, TextUtils.isTextUpperCase("brUce"));
        assertEquals(true, TextUtils.isTextUpperCase("WAYNE"));
        assertEquals(false, TextUtils.isTextUpperCase("wAYNE"));
        assertEquals(false, TextUtils.isTextUpperCase("WAyNE"));
    }
}