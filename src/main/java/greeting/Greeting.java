package greeting;

import java.util.ArrayList;

public class Greeting {
    public String greet(String name) {
    	if (TextUtils.isTextUpperCase(name)) {
			return "HELLO " + name + "!";
		}
		
		return "Hello, " + name + ".";
    }

    public String greet() {
        return "Hello, my friend.";
    }

	public String greet(ArrayList<String> people) {
		if (people.size() == 1) {
			return this.greet(people.get(0));
		}

		String greet = "Hello";
		Integer last = people.size() - 1;
		
		for (int i = 0; i < last; i++) {
			greet += ", " + people.get(i);
		}

		if (people.size() > 2) {
			greet += ",";
		}

		greet += " and " + people.get(last);
		
		return greet + ".";
	}
}
