package greeting;

public abstract class TextUtils {
	public static Boolean isTextUpperCase(String text) {
		return text == text.toUpperCase();
	}
}
